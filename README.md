# Lomiri Terminal

A terminal app for desktop and mobile devices. It provides access to terminal
sessions and is based on QMLTermWidget.

## Building the app

The app can be built from source using CMake. It depends on Qt, PAM and
QMLTermWidget.  click packages can be built using
[clickable](https://clickable-ut.dev/en/latest/).

## i18n: Translating lomiri-terminal-app into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/lomiri-terminal-app

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
