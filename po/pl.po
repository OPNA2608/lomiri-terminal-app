# Polish translation for ubuntu-terminal-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-terminal-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-terminal-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-10-20 09:02+0000\n"
"PO-Revision-Date: 2019-11-23 20:36+0000\n"
"Last-Translator: Daniel Frańczak <danielfranczak5@wp.pl>\n"
"Language-Team: Polish <https://translate.ubports.com/projects/ubports/"
"terminal-app/pl/>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2016-02-08 05:36+0000\n"

#: ../src/app/qml/AlternateActionPopover.qml:68
msgid "Select"
msgstr "Wybierz"

#: ../src/app/qml/AlternateActionPopover.qml:73
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:176
msgid "Copy"
msgstr "Kopiuj"

#: ../src/app/qml/AlternateActionPopover.qml:79
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:181
msgid "Paste"
msgstr "Wklej"

#: ../src/app/qml/AlternateActionPopover.qml:86
msgid "Split horizontally"
msgstr "Podziel poziomo"

#: ../src/app/qml/AlternateActionPopover.qml:92
msgid "Split vertically"
msgstr "Podziel pionowo"

#: ../src/app/qml/AlternateActionPopover.qml:99
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:151
#: ../src/app/qml/TabsPage.qml:32
msgid "New tab"
msgstr "Nowa karta"

#: ../src/app/qml/AlternateActionPopover.qml:104
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:146
msgid "New window"
msgstr "Nowe okno"

#: ../src/app/qml/AlternateActionPopover.qml:109
msgid "Close App"
msgstr "Zamknij aplikację"

#: ../src/app/qml/AuthenticationDialog.qml:25
msgid "Authentication required"
msgstr "Wymagane jest uwierzytelnienie"

#: ../src/app/qml/AuthenticationDialog.qml:27
msgid "Enter passcode or passphrase:"
msgstr "Wprowadź hasło:"

#: ../src/app/qml/AuthenticationDialog.qml:48
msgid "passcode or passphrase"
msgstr "hasło"

#: ../src/app/qml/AuthenticationDialog.qml:58
msgid "Authenticate"
msgstr "Uwierzytelnij"

#: ../src/app/qml/AuthenticationDialog.qml:70
#: ../src/app/qml/ConfirmationDialog.qml:42
msgid "Cancel"
msgstr "Anuluj"

#: ../src/app/qml/AuthenticationService.qml:64
msgid "Authentication failed"
msgstr "Uwierzytelnienie nie powiodło się"

#: ../src/app/qml/AuthenticationService.qml:83
msgid "No SSH server running."
msgstr "Brak uruchomionego serwera SSH."

#: ../src/app/qml/AuthenticationService.qml:84
msgid "SSH server not found. Do you want to proceed in confined mode?"
msgstr ""
"Nie znaleziono serwera SSH. Czy chcesz kontynuować w trybie ograniczonym?"

#: ../src/app/qml/ConfirmationDialog.qml:32
msgid "Continue"
msgstr "Kontynuuj"

#: ../src/app/qml/KeyboardBar.qml:197
msgid "Change Keyboard"
msgstr "Zmiana klawiatury"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:35
msgid "Control Keys"
msgstr "Klawisze CTRL"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:38
msgid "Function Keys"
msgstr "Klawisze funkcyjne"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:41
msgid "Scroll Keys"
msgstr "Klawisze przewijania"

#. TRANSLATORS: This a keyboard layout name
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:44
msgid "Command Keys"
msgstr "Klawisze komend"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:52
msgid "Ctrl"
msgstr "Ctrl"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:55
msgid "Fn"
msgstr "Fn"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:58
msgid "Scr"
msgstr "Scr"

#. TRANSLATORS: This the short display name of a keyboard layout. It should be no longer than 4 characters!
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:61
msgid "Cmd"
msgstr "Cmd"

#. TRANSLATORS: This is the name of the Control key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:78
msgid "CTRL"
msgstr "CTRL"

#. TRANSLATORS: This is the name of the Alt key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:81
msgid "Alt"
msgstr "Alt"

#. TRANSLATORS: This is the name of the Shift key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:84
msgid "Shift"
msgstr "Shift"

#. TRANSLATORS: This is the name of the Escape key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:97
msgid "Esc"
msgstr "Esc"

#. TRANSLATORS: This is the name of the Page Up key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:100
msgid "PgUp"
msgstr "PgUp"

#. TRANSLATORS: This is the name of the Page Down key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:103
msgid "PgDn"
msgstr "PgDn"

#. TRANSLATORS: This is the name of the Delete key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:106
msgid "Del"
msgstr "Del"

#. TRANSLATORS: This is the name of the Home key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:109
msgid "Home"
msgstr "Home"

#. TRANSLATORS: This is the name of the End key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:112
msgid "End"
msgstr "End"

#. TRANSLATORS: This is the name of the Tab key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:115
msgid "Tab"
msgstr "Tab"

#. TRANSLATORS: This is the name of the Enter key.
#: ../src/app/qml/KeyboardRows/JsonTranslator.qml:118
msgid "Enter"
msgstr "Enter"

#: ../src/app/qml/NotifyDialog.qml:25
msgid "OK"
msgstr "OK"

#: ../src/app/qml/Settings/BackgroundOpacityEditor.qml:26
msgid "Background opacity:"
msgstr "Nieprzezroczystość tła:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:79
msgid "R:"
msgstr "R:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:87
msgid "G:"
msgstr "G:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:95
msgid "B:"
msgstr "B:"

#: ../src/app/qml/Settings/ColorPickerPopup.qml:99
msgid "Undo"
msgstr "Cofnij"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:55
msgid "Text"
msgstr "Text"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:66
msgid "Font:"
msgstr "Czcionka:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:91
msgid "Font Size:"
msgstr "Rozmiar czcionki:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:119
msgid "Colors"
msgstr "Kolory"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:124
msgid "Ubuntu"
msgstr "Ubuntu"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:125
msgid "Green on black"
msgstr "Zielony na czarnym tle"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:126
msgid "White on black"
msgstr "Biały na czarnym tle"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:127
msgid "Black on white"
msgstr "Czarny na białym tle"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:128
msgid "Black on random light"
msgstr "Czarny na losowym świetle"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:129
msgid "Linux"
msgstr "Linux"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:130
msgid "Cool retro term"
msgstr "Cool retro term"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:131
msgid "Dark pastels"
msgstr "Ciemne pastele"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:132
msgid "Black on light yellow"
msgstr "Czarny na jasnożółtym"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:133
msgid "Customized"
msgstr "Dostosowane"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:144
msgid "Background:"
msgstr "Tło:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:152
msgid "Text:"
msgstr "Tekst:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:164
msgid "Normal palette:"
msgstr "Normalna paleta:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:171
msgid "Bright palette:"
msgstr "Jasna paleta:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:180
msgid "Preset:"
msgstr "Szablony:"

#: ../src/app/qml/Settings/SettingsInterfaceSection.qml:197
msgid "Layouts"
msgstr "Układy"

#: ../src/app/qml/Settings/SettingsPage.qml:33
msgid "close"
msgstr "zamknij"

#: ../src/app/qml/Settings/SettingsPage.qml:39
msgid "Preferences"
msgstr "Ustawienia"

#: ../src/app/qml/Settings/SettingsPage.qml:50
msgid "Interface"
msgstr "Interfejs"

#: ../src/app/qml/Settings/SettingsPage.qml:54
msgid "Shortcuts"
msgstr "Skróty"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:60
#, qt-format
msgid "Showing %1 of %2"
msgstr "Pokazuje %1 z %2"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:145
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:150
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:155
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:160
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:165
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:170
msgid "File"
msgstr "Plik"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:156
msgid "Close terminal"
msgstr "Zamknij terminal"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:161
msgid "Close all terminals"
msgstr "Zamknij wszystkie terminale"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:166
msgid "Previous tab"
msgstr "Poprzednia zakładka"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:171
msgid "Next tab"
msgstr "Następna zakładka"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:175
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:180
msgid "Edit"
msgstr "Edytuj"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:185
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:190
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:195
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:200
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:205
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:210
#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:215
msgid "View"
msgstr "Widok"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:186
msgid "Toggle fullscreen"
msgstr "Przełącz tryb pełnoekranowy"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:191
msgid "Split terminal horizontally"
msgstr "Podziel terminal w poziomie"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:196
msgid "Split terminal vertically"
msgstr "Podziel terminal w pionie"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:201
msgid "Navigate to terminal above"
msgstr "Przejdź do terminala powyżej"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:206
msgid "Navigate to terminal below"
msgstr "Przejdź do terminala poniżej"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:211
msgid "Navigate to terminal on the left"
msgstr "Przejdź do terminala po lewej"

#: ../src/app/qml/Settings/SettingsShortcutsSection.qml:216
msgid "Navigate to terminal on the right"
msgstr "Przejdź do terminala po prawej"

#: ../src/app/qml/Settings/SettingsWindow.qml:26
msgid "Terminal Preferences"
msgstr "Ustawienia terminala"

#: ../src/app/qml/Settings/ShortcutRow.qml:78
msgid "Enter shortcut…"
msgstr "Wprowadź skrót…"

#: ../src/app/qml/Settings/ShortcutRow.qml:80
msgid "Disabled"
msgstr "Wyłączony"

#: ../src/app/qml/TabsPage.qml:26
msgid "Tabs"
msgstr "Karty"

#: ../src/app/qml/TerminalPage.qml:63 ubuntu-terminal-app.desktop.in.in.h:1
msgid "Terminal"
msgstr "Terminal"

#: ../src/app/qml/TerminalPage.qml:302
msgid "Selection Mode"
msgstr "Tryb zaznaczania"

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:291
#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:320
msgid "Un-named Color Scheme"
msgstr "Nienazwany zestaw kolorów"

#: ../src/plugin/qmltermwidget/lib/ColorScheme.cpp:468
msgid "Accessible Color Scheme"
msgstr "Dostępny zestaw kolorów"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:533
msgid "Open Link"
msgstr "Otwórz odnośnik"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:534
msgid "Copy Link Address"
msgstr "Kopiuj adres odnośnika"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:538
msgid "Send Email To…"
msgstr "Wyślij email do…"

#: ../src/plugin/qmltermwidget/lib/Filter.cpp:539
msgid "Copy Email Address"
msgstr "Kopiuj adres e-mail"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:39
msgid "Match case"
msgstr "Rozróżniaj wielkość liter"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:45
msgid "Regular expression"
msgstr "Wyrażenie regularne"

#: ../src/plugin/qmltermwidget/lib/SearchBar.cpp:49
msgid "Higlight all matches"
msgstr "Podświetl wszystkie zaznaczenia"

#: ../src/plugin/qmltermwidget/lib/Vt102Emulation.cpp:982
msgid ""
"No keyboard translator available.  The information needed to convert key "
"presses into characters to send to the terminal is missing."
msgstr ""
"Tłumacz klawiatury nie jest dostępny. Brak informacji niezbędnych do zamiany "
"sygnału wciśniętego klawisza na znaki wysyłane do terminala."

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:406
msgid "Color Scheme Error"
msgstr "Błąd zestawu kolorów"

#: ../src/plugin/qmltermwidget/lib/qtermwidget.cpp:407
#, qt-format
msgid "Cannot load color scheme: %1"
msgstr "Nie można załadować zestawu kolorów: %1"

#~ msgid "Color Scheme"
#~ msgstr "Zestaw kolorów"

#~ msgid "FNS"
#~ msgstr "FUNK"

#~ msgid "SCR"
#~ msgstr "PRZE"

#~ msgid "CMD"
#~ msgstr "KOM"

#~ msgid "ALT"
#~ msgstr "ALT"

#~ msgid "SHIFT"
#~ msgstr "SHIFT"

#~ msgid "ESC"
#~ msgstr "ESC"

#~ msgid "PG_UP"
#~ msgstr "PG_UP"

#~ msgid "PG_DN"
#~ msgstr "PG_DN"

#~ msgid "DEL"
#~ msgstr "DEL"

#~ msgid "HOME"
#~ msgstr "HOME"

#~ msgid "END"
#~ msgstr "END"

#~ msgid "TAB"
#~ msgstr "TAB"

#~ msgid "ENTER"
#~ msgstr "ENTER"

#~ msgid "Login"
#~ msgstr "Zaloguj"

#~ msgid "Require login"
#~ msgstr "Wymagaj zalogowania"

#~ msgid "Enable or disable login at start"
#~ msgstr "Włącz lub wyłącz logowanie przy uruchomieniu"

#~ msgid "Ok"
#~ msgstr "OK"

#~ msgid "Settings"
#~ msgstr "Ustawienia"

#~ msgid "Required"
#~ msgstr "Wymagane"

#~ msgid "Not required"
#~ msgstr "Nie wymagane"

#~ msgid "Show Keyboard Bar"
#~ msgstr "Pokaż pasek klawiatury"

#~ msgid "Show Keyboard Button"
#~ msgstr "Pokaż przycisk klawiatury"

#~ msgid "Enter password:"
#~ msgstr "Podaj hasło:"

#~ msgid "password"
#~ msgstr "hasło"
